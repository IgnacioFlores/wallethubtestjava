package com.ef.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
public class AccessLog {

    private Date date;
    private String ip;
    private String request;
    private Integer status;
    private String userAgent;

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
