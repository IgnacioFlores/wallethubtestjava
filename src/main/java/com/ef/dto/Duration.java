package com.ef.dto;


public enum Duration {
    DAILY(),
    HOURLY();

    Duration() {
    }

    public static Duration getByValue(String value) {
        if ("daily".equals(value)) {
            return DAILY;
        } else if ("hourly".equals(value)) {
            return HOURLY;
        } else {
            throw new IllegalStateException("This is not a valid duration criteria");
        }
    }

}
