package com.ef.dto;

import lombok.Data;

import java.util.Date;

@Data
public class BlockCriteria {

    private Date startDate;
    private Duration duration;
    private Integer threshold;
}
