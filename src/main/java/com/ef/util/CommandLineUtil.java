package com.ef.util;

import com.ef.dto.BlockCriteria;
import com.ef.dto.Duration;
import org.apache.commons.cli.*;

import java.text.SimpleDateFormat;

public class CommandLineUtil {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");

    public static CommandLine getCommandLine(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption(getAccessLogOption());
        options.addOption(getIpOption());
        options.addOption(getStartDateOption());
        options.addOption(getDurationOption());
        options.addOption(getThresholdOption());
        options.addOption(getDbUserOption());
        options.addOption(getDbPasswordOption());
        options.addOption(getDbUrlOption());

        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, args);
    }

    private static Option getAccessLogOption() {
        Option accessLogOption = new Option("al", "accesslog", true, "input access log");
        accessLogOption.setRequired(false);
        return accessLogOption;
    }

    private static Option getIpOption() {
        Option ipOption = new Option("ip", "ip", true, "input ip");
        ipOption.setRequired(false);
        return ipOption;
    }

    private static Option getStartDateOption() {
        Option startDateOption = new Option("sd", "startDate", true, "input start date");
        startDateOption.setRequired(false);
        return startDateOption;
    }

    private static Option getDurationOption() {
        Option durationOption = new Option("d", "duration", true, "input duration");
        durationOption.setRequired(false);
        return durationOption;
    }

    private static Option getThresholdOption() {
        Option thresholdOption = new Option("t", "threshold", true, "input threshold");
        thresholdOption.setRequired(false);
        return thresholdOption;
    }

    private static Option getDbUserOption() {
        Option dbUsernameOption = new Option("dbu", "dbUser", true, "input dbUser");
        dbUsernameOption.setRequired(false);
        return dbUsernameOption;
    }

    private static Option getDbPasswordOption() {
        Option dbPasswordOption = new Option("dbp", "dbPassword", true, "input dbPassword");
        dbPasswordOption.setRequired(false);
        return dbPasswordOption;
    }

    private static Option getDbUrlOption() {
        Option dbUrlOption = new Option("dburl", "dbUrl", true, "input dbUrl");
        dbUrlOption.setRequired(false);
        return dbUrlOption;
    }

    public static BlockCriteria getBlockCriteria(CommandLine commandLine) throws java.text.ParseException {
        BlockCriteria blockCriteria = new BlockCriteria();
        if (null != commandLine.getOptionValue("startDate")) {
            blockCriteria.setStartDate(dateFormat.parse(commandLine.getOptionValue("startDate")));
        }
        if (null != commandLine.getOptionValue("duration")) {
            blockCriteria.setDuration(Duration.getByValue(commandLine.getOptionValue("duration")));
        }
        if (null != commandLine.getOptionValue("threshold")) {
            blockCriteria.setThreshold(Integer.valueOf(commandLine.getOptionValue("threshold")));
        }
        return blockCriteria;
    }

    public static String getLogPath(CommandLine commandLine) {
        return commandLine.getOptionValue("accesslog");
    }

    public static String getIp(CommandLine commandLine) {
        return commandLine.getOptionValue("ip");
    }

    public static String getDbUser(CommandLine commandLine) {
        return commandLine.getOptionValue("dbUser");
    }

    public static String getDbPassword(CommandLine commandLine) {
        return commandLine.getOptionValue("dbPassword");
    }

    public static String getDbUrl(CommandLine commandLine) {
        String dbUrl = commandLine.getOptionValue("dbUrl");
        if (null != dbUrl) {
            dbUrl = dbUrl.split("\\?")[0];
            dbUrl = dbUrl.contains("/parser_schema") ? dbUrl : dbUrl + "/parser_schema";
            dbUrl = dbUrl + "?autoReconnect=true&useSSL=false&serverTimezone=UTC";
        }
        return dbUrl;
    }
}
