package com.ef.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {

    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/parser_schema?autoReconnect=true&useSSL=false&serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "";

    public Connection getConnection(String dbUrl, String dbUser, String dbPassword)
            throws ClassNotFoundException, SQLException {
        dbUrl = null == dbUrl ? DB_URL : dbUrl;
        dbUser = null == dbUser ? DB_USER : dbUser;
        dbPassword = null == dbPassword ? DB_PASS : dbPassword;
        Class.forName(JDBC_DRIVER);
        System.out.println("Connecting to database...");
        return DriverManager.getConnection(dbUrl, dbUser, dbPassword);
    }

    public void closeConnection(Connection connection) throws SQLException {
        System.out.println("Closing database connection...");
        connection.close();
    }
}
