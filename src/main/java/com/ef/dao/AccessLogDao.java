package com.ef.dao;

import com.ef.dto.AccessLog;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AccessLogDao {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void parseAndLoadLogFileToDataBase(Connection connection, String path)
            throws FileNotFoundException, ParseException, SQLException {
        System.out.println("Writing the log content in database...");
        FileInputStream inputStream = new FileInputStream(path);
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        while (scanner.hasNextLine()) {
            AccessLog accessLog = castLogLineToAccessLog(scanner.nextLine());
            insert(connection, accessLog);
        }
        System.out.println("done!");
    }

    public static List<AccessLog> getByIp(Connection connection, String ip) throws SQLException, ParseException {
        final String query = "select *\n" +
                "from access_log\n" +
                "where ip = '" + ip + "'";

        System.out.println("Getting accesses for IP: " + ip);
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        List<AccessLog> accessLogs = new ArrayList<>();
        while (rs.next()) {
            AccessLog accessLog = new AccessLog();
            accessLog.setDate(dateFormat.parse(rs.getString("date")));
            accessLog.setIp(rs.getString("ip"));
            accessLog.setRequest(rs.getString("request"));
            accessLog.setStatus(rs.getInt("status"));
            accessLog.setUserAgent(rs.getString("user_agent"));
            accessLogs.add(accessLog);
        }
        System.out.println("Done!");
        return accessLogs;
    }

    private static void insert(Connection connection, AccessLog accessLog) throws SQLException {
        final String query = "insert into access_log(date, ip, request, status, user_agent)\n" +
                "values (\n" +
                "\t'" + dateFormat.format(accessLog.getDate()) + "',\n" +
                "\t'" + accessLog.getIp() + "',\n" +
                "\t'" + accessLog.getRequest() + "',\n" +
                "\t" + accessLog.getStatus() + ",\n" +
                "\t'" + accessLog.getUserAgent() + "')\n" +
                "on duplicate key update ip = '" + accessLog.getIp() + "'";

        Statement stmt = connection.createStatement();
        stmt.executeUpdate(query);
        stmt.close();
    }

    private static AccessLog castLogLineToAccessLog(String line) throws ParseException {
        String[] data = line.split("\\|");
        AccessLog accessLog = new AccessLog();
        accessLog.setDate(dateFormat.parse(data[0]));
        accessLog.setIp(data[1]);
        accessLog.setRequest(data[2].replaceAll("\"", ""));
        accessLog.setStatus(Integer.valueOf(data[3]));
        accessLog.setUserAgent(data[4].replaceAll("\"", ""));
        return accessLog;
    }
}
