package com.ef.dao;

import com.ef.dto.BlockCriteria;
import com.ef.dto.BlockedIp;
import com.ef.dto.Duration;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BlockedIpDao {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static List<BlockedIp> getBlockedIps(Connection connection, BlockCriteria criteria) throws SQLException {

        Date endDate = getEndDate(criteria);

        final String query = "select iterations.*\n" +
                "from (\n" +
                "\tselect ip, count(*) as count\n" +
                "\tfrom access_log \n" +
                "\twhere date >= '" + dateFormat.format(criteria.getStartDate()) + "'\n" +
                "\tand date < '" + dateFormat.format(endDate) + "'\n" +
                "\tgroup by ip) iterations\n" +
                "where iterations.count >= " + criteria.getThreshold();

        System.out.println("Getting blocked IPs...");
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);

        List<BlockedIp> blockedIps = new ArrayList<>();
        while (rs.next()) {
            BlockedIp blockedIp = new BlockedIp();
            blockedIp.setIp(rs.getString("ip"));
            blockedIp.setReason("Your IP has been blocked because you have had more than "
                    + criteria.getThreshold() + " accesses between " + dateFormat.format(criteria.getStartDate())
                    + " and " + dateFormat.format(endDate));
            blockedIps.add(blockedIp);
        }
        System.out.println("Done!");
        return blockedIps;
    }

    private static Date getEndDate(BlockCriteria criteria) {
        Date endDate;
        Calendar c = Calendar.getInstance();
        c.setTime(criteria.getStartDate());
        if (criteria.getDuration().equals(Duration.DAILY)) {
            c.add(Calendar.DATE, 1);
            c.add(Calendar.SECOND, -1);
            endDate = c.getTime();
        } else {
            c.add(Calendar.HOUR, 1);
            c.add(Calendar.SECOND, -1);
            endDate = c.getTime();
        }
        return endDate;
    }

    public static void insert(Connection connection, List<BlockedIp> blockedIps) throws SQLException {
        System.out.println("Writing blocked IPs on Database...");
        for (BlockedIp blockedIp : blockedIps) {
            insert(connection, blockedIp);
        }
        System.out.println("Done!");
    }

    private static void insert(Connection connection, BlockedIp blockedIp) throws SQLException {
        final String query = "insert into blocked_ip(ip, reason)\n" +
                "values (\n" +
                "\t'" + blockedIp.getIp() + "',\n" +
                "\t'" + blockedIp.getReason() + "')\n" +
                "on duplicate key update reason = '" + blockedIp.getReason() + "'";

        Statement stmt = connection.createStatement();
        stmt.executeUpdate(query);
        stmt.close();
    }
}
