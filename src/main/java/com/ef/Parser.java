package com.ef;

import com.ef.dao.AccessLogDao;
import com.ef.dao.BlockedIpDao;
import com.ef.dto.AccessLog;
import com.ef.dto.BlockCriteria;
import com.ef.dto.BlockedIp;
import com.ef.sql.DbUtil;
import com.ef.util.CommandLineUtil;
import org.apache.commons.cli.*;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * This is the main class for the MySQL and Java test for WalletHub
 *
 * @author Ignacio Simon Flores
 */
public class Parser {

    private static Connection connection;
    private static BlockCriteria blockCriteria;
    private static String logPath;
    private static String ip;
    private static String dbUser;
    private static String dbPassword;
    private static String dbUrl;

    private static final DbUtil dbUtil = new DbUtil();

    public static void main(String[] args) throws SQLException, ClassNotFoundException, ParseException,
            org.apache.commons.cli.ParseException, FileNotFoundException {
        getCommandLineOptions(args);
        getDbConnection();
        parseAndLoadLogsToDataBase();
        getAccessLogsByIp();
        getBlockedIpsAndInsertInDb();
        closeDbConnection();
    }

    private static void getCommandLineOptions(String[] args) throws org.apache.commons.cli.ParseException, ParseException {
        CommandLine commandLine = CommandLineUtil.getCommandLine(args);
        blockCriteria = CommandLineUtil.getBlockCriteria(commandLine);
        logPath = CommandLineUtil.getLogPath(commandLine);
        ip = CommandLineUtil.getIp(commandLine);
        dbUrl = CommandLineUtil.getDbUrl(commandLine);
        dbUser = CommandLineUtil.getDbUser(commandLine);
        dbPassword = CommandLineUtil.getDbPassword(commandLine);
    }

    private static void getDbConnection() throws ClassNotFoundException, SQLException {
        connection = dbUtil.getConnection(dbUrl, dbUser, dbPassword);
    }

    private static void parseAndLoadLogsToDataBase() throws FileNotFoundException, ParseException, SQLException {
        if (null != logPath) {
            AccessLogDao.parseAndLoadLogFileToDataBase(connection, logPath);
        }
    }

    private static void getAccessLogsByIp() throws SQLException, ParseException {
        if (null != ip) {
            List<AccessLog> accessLogsByIp = AccessLogDao.getByIp(connection, ip);
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.println("-------------------------- Accesses for IP: 192.168.234.82 --------------------------");
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.println("-------------------------------------------------------------------------------------");
            for (AccessLog accessLog : accessLogsByIp) {
                System.out.println(accessLog);
            }
        }
    }

    private static void getBlockedIpsAndInsertInDb() throws SQLException {
        if (null != blockCriteria.getStartDate()
                && null != blockCriteria.getThreshold()
                && null != blockCriteria.getDuration()) {
            List<BlockedIp> blockedIps = BlockedIpDao.getBlockedIps(connection, blockCriteria);
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.println("------------------------------------ Blocked IPs ------------------------------------");
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.println("-------------------------------------------------------------------------------------");
            for (BlockedIp blockedIp : blockedIps) {
                System.out.println(blockedIp);
            }
            BlockedIpDao.insert(connection, blockedIps);
        }
    }

    private static void closeDbConnection() throws SQLException {
        dbUtil.closeConnection(connection);
    }
}
